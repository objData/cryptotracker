﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTracker.Data;
using CryptoTracker.Models;
using CryptoTracker.Services;
using Microsoft.AspNetCore.Mvc;

namespace CryptoTracker.Controllers
{
	[Route("api/[controller]")]
	public class CryptoTrackerController : Controller
	{
		[HttpGet("[action]")]
		public async Task<IEnumerable<Market>> BittrexMarkets()
		{
			var bitApi = new BittrexApiClient("", "");
			var markets = await bitApi.GetMarkets();

			return markets.Result.Take(30);
		}

		[HttpGet("[action]")]
		public async Task<MarketSummary> BittrexMarketSummary(string market)
		{
			var bitApi = new BittrexApiClient("", "");
			var marketSum = await bitApi.GetMarketSummary(market);
			if (!marketSum.Success)
				return null;

			using (var context = new CryptoTrackerEfCoreContext())
			{
				if (!context.MarketSummeries.Any(m => m.MarketName == marketSum.Result.MarketName && m.TimeStamp == marketSum.Result.TimeStamp))
				{
					context.MarketSummeries.Add(marketSum.Result);
					context.SaveChanges();
				}
			}

			return marketSum.Result;
		}
	}
}
