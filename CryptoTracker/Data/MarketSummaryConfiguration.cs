﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTracker.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoTracker.Data
{
	public class MarketSummaryConfiguration : IEntityTypeConfiguration<MarketSummary>
	{
		public void Configure(EntityTypeBuilder<MarketSummary> builder)
		{
			// Set primary key
			builder.HasKey(o =>  new {o.MarketName, o.TimeStamp});
		}
	}
}
