﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using CryptoTracker.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;

namespace CryptoTracker.Data
{
	public class CryptoTrackerEfCoreContext : DbContext
	{
		public DbSet<MarketSummary> MarketSummeries { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var dbConnString = @"Server=.\;Database=CryptoTrackerDb;Trusted_Connection=True;MultipleActiveResultSets=true";
			optionsBuilder.UseSqlServer(dbConnString);
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			// Customizations must go after base.OnModelCreating(builder)

			builder.ApplyConfiguration(new MarketSummaryConfiguration());
		}
	}
}
