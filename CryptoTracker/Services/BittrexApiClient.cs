﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CryptoTracker.Models;
using Newtonsoft.Json;

namespace CryptoTracker.Services
{
	public class BittrexApiClient : IBittrexApiClient
	{
		const string BaseAddress = "https://bittrex.com/api";
		private readonly HttpClient _httpClient;

		private readonly String _apiKey;
		private readonly String _apiSecret;

		private readonly String _publicBaseUrl;
		private readonly String _accountBaseUrl;
		private readonly String _marketBaseUrl;

		private readonly String _apiVersion = "v1.1";

		public BittrexApiClient(String apiKey, String apiSecret)
			: this(BaseAddress, apiKey, apiSecret)
		{
		}

		public BittrexApiClient(String baseAddress, String apiKey, String apiSecret)
			: this(baseAddress, apiKey, apiSecret, new HttpClient())
		{
		}

		public BittrexApiClient(String baseAddress, String apiKey, String apiSecret, HttpClient httpClient)
		{
			_httpClient = httpClient;

			_apiKey = apiKey;
			_apiSecret = apiSecret;

			_publicBaseUrl = $"{baseAddress}/{_apiVersion}/public";
			_accountBaseUrl = $"{baseAddress}/{_apiVersion}/account";
			_marketBaseUrl = $"{baseAddress}/{_apiVersion}/market";
		}

		public async Task<BittrexApiResult<Market[]>> GetMarkets()
		{
			var url = $"{_publicBaseUrl}/getmarkets";

			var json = await GetPublicAsync(url).ConfigureAwait(false);

			return JsonConvert.DeserializeObject<BittrexApiResult<Market[]>>(json);
		}

		private async Task<string> GetPublicAsync(string url)
		{
			var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));

			var response = await _httpClient.SendAsync(request).ConfigureAwait(false);
			response.EnsureSuccessStatusCode();

			return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
		}

		public async Task<BittrexApiResult<MarketSummary>> GetMarketSummary(String marketName)
		{
			var url = $"{_publicBaseUrl}/getmarketsummary?market={marketName}";

			var json = await GetPublicAsync(url).ConfigureAwait(false);

			var arrayResult = JsonConvert.DeserializeObject<BittrexApiResult<MarketSummary[]>>(json);

			// The end point returns an array so we need to pull this out into a single MarketSummary result
			return new BittrexApiResult<MarketSummary>(
				arrayResult.Success,
				arrayResult.Message,
				arrayResult.Result.FirstOrDefault());
		}
	}
}
