import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-crypto-markets',
  templateUrl: './crypto-markets.component.html'
})
export class CryptoMarketsComponent {
  public markets: CryptoMarkets[];
  public marketSummary: MarketSummary;
  public selectedRow: number;
  setClickedRow: Function;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<CryptoMarkets[]>(baseUrl + 'api/CryptoTracker/BittrexMarkets').subscribe(result => {
      this.markets = result;
    }, error => console.error(error));

    this.setClickedRow = function (index, market) {
      this.marketSummary = null;
      this.selectedRow = index;
      http.get<MarketSummary>(baseUrl + 'api/CryptoTracker/BittrexMarketSummary?market=' + market).subscribe(result => {
        this.marketSummary = result;
      }, error => console.error(error));
    } 
  }
}

interface CryptoMarkets {
  created: string;
  marketCurrency: string;
  baseCurrency: string;
  minTradeSize: number;
  marketName: string;
}

interface MarketSummary {
  timeStamp: string;
  marketName: string;
  high: number;
  low: number;
  last: number;
  volume: number;
  baseVolume: number;
  openBuyOrders: number;
  openSellOrders: number;
}
